skais
=====

[![coverage report](https://gitlab.lis-lab.fr/raphael.sturgis/skais/badges/develop/coverage.svg)](https://gitlab.lis-lab.fr/raphael.sturgis/skais/-/commits/develop)
[![pipeline status](https://gitlab.lis-lab.fr/raphael.sturgis/skais/badges/develop/pipeline.svg)](https://gitlab.lis-lab.fr/raphael.sturgis/skais/-/commits/develop)

``skais`` is a Python package for Raphael Sturgis' PhD at LIS and Searoutes.

Install
-------------

To install the package for development with ``pip``, run from the top-level
source (i.e., this file's directory)::

    pip install -e .

To run unitary tests, install required packages::

    pip install -r requirements.txt



Dataset installation
--------------------
Download the data to your favorite directory.

Then run function ``skais.Utils.config.generate_config`` in order to
create a configuration file and modify it to specify the path to your data
folder. The location of the configuration file is given by function
``skais.Utils.config.get_config_file``.

Once the configuration is set, the path to the data folder is
returned by function ``skais.Utils.config.get_data_path``.