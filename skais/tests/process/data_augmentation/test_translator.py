import unittest

from skais.ais.ais_trajectory import AISTrajectory
import pandas as pd

from skais.process.data_augmentation.translator import Translator


class TestTranslator(unittest.TestCase):
    def setUp(self):
        t1 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [0 for _ in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )
        t2 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [-12 + i for i in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )

        self.trajectories = [t1, t2]

    def test_transform_longitude(self):
        aug = Translator(1, 0)

        result = aug.transform(self.trajectories)

        t1 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [0 for _ in range(10)],
                    'longitude': [13 + i for i in range(10)]
                }
            )
        )
        t2 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [-12 + i for i in range(10)],
                    'longitude': [13 + i for i in range(10)]
                }
            )
        )
        expected = [t1, t2]

        for t1, t2 in zip(result, expected):
            pd.testing.assert_frame_equal(t1.df, t2.df)

    def test_invariance(self):
        t1 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [0 for _ in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )
        t2 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [-12 + i for i in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )

        expected = [t1, t2]

        for t1, t2 in zip(self.trajectories, expected):
            pd.testing.assert_frame_equal(t1.df, t2.df)
if __name__ == '__main__':
    unittest.main()
