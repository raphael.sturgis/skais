import unittest

from skais.ais.ais_trajectory import AISTrajectory
from skais.learn.data_augmentation import *
import pandas as pd


class data_augmentation(unittest.TestCase):
    def test_shift(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [0 for _ in range(5)] + [45 for _ in range(5)],
                    'longitude': [0 for _ in range(10)],
                    'cog': [0 for _ in range(10)],
                    'heading': [90 for _ in range(10)]
                }
            )
        )

        shifted = shift_positions(trajectory, 0, 0, np.pi / 2)
        self.assertTrue(True)

    def test_shift_2(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [0 for _ in range(10)],
                    'longitude': [-150, -120, -60, -30, 0, 30, 60, 120, 150, 180],
                    'cog': [0 for _ in range(10)],
                    'heading': [90 for _ in range(10)]
                }
            )
        )

        shifted = shift_positions(trajectory, 0, 0, np.pi / 2)
        self.assertTrue(True)

    def test_shift_3(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [0],
                    'latitude': [-45],
                    'longitude': [45],
                    'cog': [45],
                    'heading': [120]
                }
            )
        )

        shifted = shift_positions(trajectory, np.pi/2, 0, np.pi / 2)
        self.assertTrue(True)

