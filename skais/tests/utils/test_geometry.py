import unittest

import numpy as np

from skais.process.geography import PI
from skais.utils.geometry import bresenham, vectorize_angle, transpose_matrix, vector_rotation, local_ref


class TestGeometry(unittest.TestCase):
    def test_bresenham(self):
        result = bresenham(3, 4, 16, 9)
        expected = [(3, 4), (4, 4), (5, 5), (6, 5), (7, 6), (8, 6), (9, 6), (10, 7), (11, 7), (12, 7), (13, 8), (14, 8),
                    (15, 9), (16, 9)]

        self.assertListEqual(result, expected)

    def test_bresenham_inverted(self):
        result = bresenham(16, 9, 3, 4)
        expected = [(3, 4), (4, 4), (5, 5), (6, 5), (7, 6), (8, 6), (9, 6), (10, 7), (11, 7), (12, 7), (13, 8),
                    (14, 8), (15, 9), (16, 9)]

        self.assertListEqual(result, expected)

    def test_bresenham_inverted_2(self):
        result = bresenham(16, 4, 3, 9)
        expected = [(3, 9), (4, 9), (5, 8), (6, 8), (7, 7), (8, 7), (9, 7), (10, 6), (11, 6), (12, 6), (13, 5), (14, 5),
                    (15, 4), (16, 4)]
        self.assertListEqual(result, expected)

    def test_bresenham_same_line(self):
        result = bresenham(3, 4, 10, 4)
        expected = [(3, 4), (4, 4), (5, 4), (6, 4), (7, 4), (8, 4), (9, 4), (10, 4)]

        self.assertListEqual(result, expected)

    def test_bresenham_same_column(self):
        result = bresenham(3, 4, 3, 10)
        expected = [(3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10)]

        self.assertListEqual(result, expected)

    def test_vectorize_angle_0(self):
        vectorized_angle = vectorize_angle(0)
        np.testing.assert_almost_equal(vectorized_angle, np.array([1, 0, 0]))

    def test_vectorize_angle_half_PI(self):
        vectorized_angle = vectorize_angle(PI / 2)
        np.testing.assert_almost_equal(vectorized_angle, np.array([0, 1, 0]))

    def test_vectorize_angle_PI(self):
        vectorized_angle = vectorize_angle(PI)
        np.testing.assert_almost_equal(vectorized_angle, np.array([-1, 0, 0]))

    def test_transpose_matrix_1(self):
        origin = np.array(
            [[1, 0, 0],
             [0, 1, 0],
             [0, 0, 1]]
        )
        target = np.array(
            [[1, 0, 0],
             [0, 1, 0],
             [0, 0, 1]]
        )
        result = transpose_matrix(origin, target)
        expected = np.array(
            [[1, 0, 0],
             [0, 1, 0],
             [0, 0, 1]]
        )
        np.testing.assert_almost_equal(result, expected)

    def test_transpose_matrix_2(self):
        origin = np.array(
            [[1, 0, 0],
             [0, 1, 0],
             [0, 0, 1]]
        )
        target = np.array(
            [[0, 1, 0],
             [1, 0, 0],
             [0, 0, 1]]
        )
        result = transpose_matrix(origin, target)
        expected = np.array(
            [[0, 1, 0],
             [1, 0, 0],
             [0, 0, 1]]
        )
        np.testing.assert_almost_equal(result, expected)

    def test_rotate_vector(self):
        result = vector_rotation(np.array([0, 0, 1]), 0, 0, np.pi / 2)

        np.testing.assert_almost_equal(result, np.array([0, -1, 0]))

    def test_local_ref_0(self):
        result = local_ref(0, 0)

        np.testing.assert_almost_equal(result, np.array([[0, 0, -1],
                                                         [0, 1, 0],
                                                         [1, 0, 0]]))

    def test_local_ref_1(self):
        result = local_ref(np.pi/2, 0)

        np.testing.assert_almost_equal(result, np.array([[1, 0, 0],
                                                         [0, 1, 0],
                                                         [0, 0, 1]]))

if __name__ == '__main__':
    unittest.main()
