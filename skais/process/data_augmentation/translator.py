from skais.ais.ais_trajectory import AISTrajectory
from skais.process.data_augmentation.data_transformer import DataTransformer


class Translator(DataTransformer):
    def __init__(self, longitude, latitude):
        self.longitude = longitude
        self.latitude = latitude

    def transform(self, x):
        result = []
        for trajectory in x:
            df = trajectory.df.copy()
            df['longitude'] = trajectory.df['longitude'] + self.longitude
            result.append(AISTrajectory(df))
        return result
