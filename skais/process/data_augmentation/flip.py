from skais.ais.ais_trajectory import AISTrajectory
from skais.process.data_augmentation.data_transformer import DataTransformer


class Flip(DataTransformer):
    def __init__(self, meridian=None, parallel=None):
        self.meridian = meridian
        self.parallel = parallel

    def transform(self, x):
        result = []
        if self.parallel is not None:
            for trajectory in x:
                df = trajectory.df.copy()
                df['latitude'] = -trajectory.df['latitude']
                result.append(AISTrajectory(df))
        return result
