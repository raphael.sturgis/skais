from skais.process.data_augmentation.data_transformer import DataTransformer


class Pipeline(DataTransformer):
    def __init__(self, sequence):
        for s in sequence:
            assert (isinstance(s, DataTransformer))

        self.sequence = sequence

    def transform(self, x):
        result = x.copy()
        for aug in self.sequence:
            result = aug.transform(result)
        return result
