import random
import numpy as np
from skais.learn.data_augmentation import shift_positions


class RandomAxisRotationEngine:
    def __init__(self, shifts_per_trajectory=1):
        self.shifts_per_trajectory = shifts_per_trajectory

    def apply(self, trajectories, shuffle=True):
        new_trajectories = []
        for trajectory in trajectories:
            for _ in range(self.shifts_per_trajectory):
                new_trajectories.append(shift_positions(trajectory, random.uniform(0, 1)*np.pi - np.pi/2,
                                                        2*(random.uniform(0, 1)*np.pi) - np.pi,
                                                        2*(random.uniform(0, 1)*np.pi) - np.pi))
        if not shuffle:
            return new_trajectories
        else:
            return random.shuffle(new_trajectories)
