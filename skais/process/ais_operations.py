from skais.ais.ais_trajectory import AISTrajectory

# Trajectories
"""
    Separates AISPoints into individual trajectories
"""


def get_trajectories(ais_points):
    trajectories = []
    for mmsi in ais_points.df.mmsi.unique():
        trajectories.append(AISTrajectory(ais_points.df[ais_points.df['mmsi'] == mmsi].reset_index(drop=True),
                                          mmsi=mmsi))
    return trajectories
