import math

import numpy as np
from numba import jit

PI = math.pi


@jit(nopython=True)
def to_rad(degree):
    return (degree / 360.0) * (2 * PI)


@jit(nopython=True)
def to_deg(rad):
    return (rad * 360.0) / (2 * PI)


@jit(nopython=True)
def bearing(p1, p2):
    long1 = to_rad(p1[0])
    long2 = to_rad(p2[0])

    lat1 = to_rad(p1[1])
    lat2 = to_rad(p2[1])

    y = np.sin(long2 - long1) * np.cos(lat2)
    x = np.cos(lat1) * np.sin(lat2) - np.sin(lat1) * np.cos(lat2) * np.cos(long2 - long1)
    return to_deg(np.arctan2(y, x))


@jit(nopython=True)
def angle_between_three_points(p1, p2, p3):
    alpha = bearing(p2, p1)
    beta = bearing(p2, p3)
    result = alpha - beta
    if result > 180:
        return 180 - result
    else:
        return result


def compute_point_angles(dat):
    angles = np.zeros(dat.shape[0])

    p1 = (dat[0][0], dat[0][1])
    p2 = (dat[1][0], dat[1][1])
    angles[0] = bearing(p1, p2)
    for i in range(1, dat.shape[0] - 1):
        p1 = (dat[i - 1][0], dat[i - 1][1])
        p2 = (dat[i][0], dat[i][1])
        p3 = (dat[i + 1][0], dat[i + 1][1])

        angles[i] = angle_between_three_points(p1, p2, p3)
    return angles
