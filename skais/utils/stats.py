import math

import ot
from scipy.spatial.distance import jensenshannon
import numpy as np


def compare_label_distributions(distribution1, distribution2):
    assert len(distribution1) == len(distribution2)
    n_classes = len(distribution1)
    if sum(distribution1) != 1:
        distribution1 = distribution1 / sum(distribution1)
    if sum(distribution2) != 1:
        distribution2 = distribution2 / sum(distribution2)
    return ot.emd2(distribution1, distribution2,
                   [[1 if i != j else 0 for i in range(n_classes)] for j in range(n_classes)])


def compare_x_knowing_y_jsd(d1, d2):
    jsd = 0
    for h1, h2 in zip(d1, d2):
        for i in range(h1.shape[0]):
            jsd += jensenshannon(h1[i, :], h2[i, :])
    return jsd / (len(d1) * d1[0].shape[0])


def calc_std_dev(angles):
    sin = 0
    cos = 0
    for angle in angles:
        sin += np.sin(angle * (math.pi / 180.0))
        cos += np.cos(angle * (math.pi / 180.0))

    sin /= len(angles)
    cos /= len(angles)

    stddev = math.sqrt(-math.log(min(1, sin * sin + cos * cos)))

    return stddev
