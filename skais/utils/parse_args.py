def parseToken(current_token, head, isTokenNumber):
    if isTokenNumber:
        head.append(float(current_token))
        return ''
    return current_token

def parseListOfList(string):
    openBrackets = 0
    stack = []
    head = []
    current_token = ''
    isTokenNumber = True
    for s in string:
        if s == '[':
            openBrackets +=1
            stack.append(head)
            head = []

        elif s == ']':
            current_token = parseToken(current_token, head, isTokenNumber)

            openBrackets -=1
            tmp = stack.pop()
            tmp.append(head)
            head = tmp
            isTokenNumber = False

        elif s == ",":
            current_token = parseToken(current_token, head, isTokenNumber)

        else:
            isTokenNumber = True
            current_token += s


    return head[0]