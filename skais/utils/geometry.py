import numpy as np
from numba import jit
import math as m


@jit(nopython=True)
def dist_on_grid(x1, y1, x2, y2):
    dx = int(x2 - x1)
    dy = int(y2 - y1)
    return np.sqrt(dx ** 2 + dy ** 2)


@jit(nopython=True)
def bresenham(x1, y1, x2, y2):
    dx = int(x2 - x1)
    dy = int(y2 - y1)

    sx = sy = 1
    if dx < 0:
        sx = -1
    if dy < 0:
        sy = -1

    pixels = []
    if abs(dx) > abs(dy):  # slope < 1
        if x1 > x2:
            tmp = x2
            x2 = x1
            x1 = tmp

            tmp = y2
            y2 = y1
            y1 = tmp
            sy *= -1

        y = y1
        p = (2 * abs(dy)) - abs(dx)
        pixels.append((x1, y1))

        for x in range(x1 + 1, x2 + 1):
            if p < 0:
                p += 2 * abs(dy)
            else:
                y += sy
                p += (2 * abs(dy)) - (2 * abs(dx))
            pixels.append((x, y))
    else:  # slope >= 1
        if y1 > y2:
            tmp = x2
            x2 = x1
            x1 = tmp

            tmp = y2
            y2 = y1
            y1 = tmp
            sx *= -1
        x = x1

        pixels.append((x1, y1))
        p = (2 * abs(dx)) - abs(dy)
        for y in range(y1 + 1, y2 + 1):
            if p < 0:
                p += 2 * abs(dx)
            else:
                x += sx
                p += (2 * abs(dx)) - (2 * abs(dy))
            pixels.append((x, y))

    return pixels

@jit(nopython=True, cache=True)
def vectorize_angle(angle):
    return np.array([-np.cos(angle), np.sin(angle), 0])


@jit(nopython=True, cache=True)
def angleize_vector(vector):
    if vector[2] > 0.0001:
        raise ValueError
    return np.arctan2(vector[1], -vector[0])


@jit(nopython=True, cache=True)
def local_ref(lat, long):
    theta = (np.pi / 2) - lat
    phi = long

    return np.array(
        [
            [np.cos(theta) * np.cos(phi), np.cos(theta) * np.sin(phi), -np.sin(theta)],
            [-np.sin(phi), np.cos(phi), 0],
            [np.sin(theta) * np.cos(phi), np.sin(theta) * np.sin(phi), np.cos(theta)]
        ]
    )


# @jit(nopython=True, cache=True)
def transpose_matrix(origin_ref, target_ref):
    i1 = origin_ref[0, :]
    j1 = origin_ref[1, :]
    k1 = origin_ref[2, :]
    i2 = target_ref[0, :]
    j2 = target_ref[1, :]
    k2 = target_ref[2, :]

    return np.array(
        [
            [i1.dot(i2), j1.dot( i2), k1.dot(i2)],
            [i1.dot(j2), j1.dot(j2), k1.dot(j2)],
            [i1.dot(k2), j1.dot(k2), k1.dot(k2)]
        ]
    )

    # return np.matmul(origin_ref, target_ref)


@jit(nopython=True, cache=True)
def rotation_quaternions(lat, long, angle):
    theta = (np.pi / 2) - lat
    phi = long
    vx = np.cos(phi) * np.sin(theta)
    vy = np.sin(phi) * np.sin(theta)
    vz = np.cos(theta)

    return (np.array([np.cos(angle / 2), vx * np.sin(angle / 2), vy * np.sin(angle / 2), vz * np.sin(angle / 2)]),
            np.array([np.cos(angle / 2), -vx * np.sin(angle / 2), -vy * np.sin(angle / 2), -vz * np.sin(angle / 2)]))


@jit(nopython=True, cache=True)
def quaternion_multiply(quaternion0, quaternion1):
    w0, x0, y0, z0 = quaternion0
    w1, x1, y1, z1 = quaternion1
    return np.array([(w0 * w1) - (x0 * x1) - (y0 * y1) - (z0 * z1),
                     (w0 * x1) + (x0 * w1) + (y0 * z1) - (z0 * y1),
                     (w0 * y1) - (x0 * z1) + (y0 * w1) + (z0 * x1),
                     (w0 * z1) + (x0 * y1) - (y0 * x1) + (z0 * w1)])


@jit(nopython=True, cache=True)
def vector_rotation(vect, lat, long, angle):
    q, q_conjugate = rotation_quaternions(lat, long, angle)
    v = np.insert(vect, 0, 0)

    return quaternion_multiply(quaternion_multiply(q, v), q_conjugate)[1:]


@jit(nopython=True, cache=True)
def spherical_to_carthesian(lat, long):
    theta = (np.pi / 2) - lat
    phi = long
    return np.array([np.cos(phi) * np.sin(theta), np.sin(phi) * np.sin(theta), np.cos(theta)])


@jit(nopython=True, cache=True)
def carthesian_to_spherical(vect):
    x = vect[0]
    y = vect[1]
    z = vect[2]
    XsqPlusYsq = x ** 2 + y ** 2
    return m.atan2(z, m.sqrt(XsqPlusYsq)), m.atan2(y, x)


@jit(nopython=True, cache=True)
def rotate_vector_by_quaternion(vector, quaternion, inverse_quaternion):
    vect = np.zeros(4)
    vect[1:] = vector
    return quaternion_multiply(quaternion_multiply(quaternion, vect), inverse_quaternion)[1:]
