import numpy as np


def histogram(ais_points, features, bins, ranges=None, label=None, y_field='label'):
    if label is not None:
        tmp = ais_points.df[ais_points.df[y_field] == label]
    else:
        tmp = ais_points.df
    dat = tmp[features]
    h = np.histogramdd(dat.to_numpy(), bins, ranges)[0]
    if h.sum() == 0:
        return np.full(h.shape, 1 / h.size)
    else:
        return h / h.sum()


def disjointed_histogram(ais_points, features, bins, ranges, label=None, y_field='label'):
    if label is not None:
        tmp = ais_points.df[ais_points.df[y_field] == label]
    else:
        tmp = ais_points.df

    if type(bins) == int:
        bins = [bins for _ in features]

    histograms = []
    for feature, h_bin, f_range in zip(features, bins, ranges):
        histograms.append(np.histogram(tmp[feature], h_bin, f_range))
    return histograms


def histogram_joint_x_y(ais_points, x_fields, x_nb_bins, x_range, y_fields, y_nb_bins, y_range):
    return histogram(ais_points, x_fields + [y_fields],
                     bins=[x_nb_bins for _ in x_fields] + [y_nb_bins],
                     ranges=x_range + [y_range])


def histogram_x_knowing_y(ais_points, x_fields, x_nb_bins, x_range, y_nb_bins, y_field):
    result = []
    for i in range(y_nb_bins):
        layer = histogram(ais_points, x_fields, bins=x_nb_bins, ranges=x_range, label=i, y_field=y_field)
        result.append(layer)
    return np.stack(result, axis=len(x_fields))


def disjointed_histogram_x_knowing_y(ais_points, features, x_nb_bins, x_range, y_nb_bins, y_field):
    out = []
    for feature, f_range in zip(features, x_range):
        result = []
        for i in range(y_nb_bins):
            layer, _ = np.histogram(ais_points.df[ais_points.df[y_field] == i][feature].to_numpy(), bins=x_nb_bins,
                                    range=f_range)
            if layer.sum() == 0:
                layer = np.full(layer.shape, 1)
            result.append(layer)
        out.append(np.stack(result))
    return out


def histogram_y_knowing_x(ais_points, x_fields, x_nb_bins, x_range, y_nb_bins, y_field, y_range):
    h_joint = histogram_joint_x_y(ais_points, x_fields, x_nb_bins, x_range, y_nb_bins, y_field, y_range)
    y_hist = histogram(ais_points, features=y_field, bins=y_nb_bins, ranges=[y_range])

    result = np.zeros(h_joint.shape)

    for idx, x in np.ndenumerate(h_joint):
        if h_joint[idx[:-1]].sum() == 0:
            result[idx] = y_hist[idx[-1]]
        else:
            result[idx] = x / h_joint[idx[:-1]].sum()
    return result
